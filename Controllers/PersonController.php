<?php 
use Philo\Blade\Blade;
class PersonController {
    public function index(){
       View("person");     
    }
   
    public function search(){
        require_once "../bootstrap.php";
       
        if(!empty($_POST['personid'])){
            $perid = $_POST['personid'];
        }else{
            $perid = null;
        }
        if(!empty($_POST['eduLevel'])){
            $eduLv = $_POST['eduLevel'];
        }else{
            $eduLv = null;
        }
       $query = $entityManager->createQuery('SELECT p.personId, t.titleShortName ,p.fname ,p.lname FROM Person p LEFT JOIN p.title t
                                                WHERE  (p.personId = :perid OR :perid IS NULL) 
                                                 AND ( p.fname LIKE :fname OR :fname IS NULL) 
                                                 AND ( p.lname LIKE :lname OR :lname IS NULL)
             /*ทำความเขาใจอีกครั้งส่วนนี้*/            AND ( p.personId   IN (SELECT pp.personId  FROM  PersonEducation e LEFT JOIN  e.person pp WHERE e.level = :eduLv OR :eduLv IS NULL) )');
       
       $query->setParameters(array('perid'=> $perid, 'fname'=>'%'.$_POST['Fname'].'%', 'lname'=>'%'.$_POST['Lname'].'%', 'eduLv'=>$eduLv) );
   /* $query->setParameters(array('perid'=> $perid, 'fname'=>'%'.$_POST['Fname'].'%', 'lname'=>'%'.$_POST['Lname'].'%') );   เพิ่มคิวรี่ edulv  */
        $result = $query->getResult();
        if($result != null){
        for ($i=0 ; $i<count($result); $i++) {
            $data[] = array (
                'person_id' => $result[$i]['personId'],
                'title_id' => $result[$i]['titleShortName'],
                'fname' => $result[$i]['fname'],
                'lname' => $result[$i]['lname'],
                'eduperson' => $result[$i]['personId'],
            );
        }   

        $length = $_POST["length"];
        $start = $_POST["start"];  
        $total = count($data);   
        $output = array_slice($data, $start,$length);       
        $alldata = array (
            'data' =>  $output,
            "recordsTotal"    => intval( $total ),
            "recordsFiltered" => intval( $total ),
        );

        echo json_encode($alldata);   
     
    }else{
       $result[] = null ;
       echo json_encode($result);   
     
    }     

}

     public function eduLevel(){
        require_once "../bootstrap.php";

        $query =$entityManager->createQuery('SELECT e.levelId, e.levelName FROM EducationLevel e');
        $result = $query->getResult();
        for ($i=0 ; $i<count($result); $i++) {
            $data[] = array (
                'id' => $result[$i]['levelId'],
                'text' => $result[$i]['levelName'],
                
            );
        }
   
        echo json_encode($data);
    }


//for detail PSN_EDU

    public function getPersonEdu(){
        require_once "../bootstrap.php";
    $query = $entityManager->createQuery('SELECT e.personEducationId, e.educationName,l.levelName FROM PersonEducation e LEFT JOIN e.level l  WHERE e.person = :perid ');
    $query->setParameters(array('perid'=>$_POST['personid']) );
    $rs_personEdu = $query->getResult();
  if($rs_personEdu != null){
    for($i=0; $i<count($rs_personEdu); $i++){
        $rs_data[] = array(
            'rsd_personEdu' =>$i+1,
            'rsd_level' => $rs_personEdu[$i]['levelName'],
            'rsd_personEduName' => $rs_personEdu[$i]['educationName']

        );
    }
    $length = $_POST["length"];
    $start = $_POST["start"];  
    $total = count($rs_data);   
    $output = array_slice($rs_data, $start,$length);   
    $data = array(
        'data' => $rs_data,
        "recordsTotal"    => intval( $total ),
        "recordsFiltered" => intval( $total )
    );
    echo json_encode($data);
}else{
    
        $rs_personEdu[] = null ;
        echo json_encode($rs_personEdu);   
     
}
}

}
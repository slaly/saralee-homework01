@extends('layout')

@section('content')
<form action= "#" method="POST" id ="frmSearch">
    <div class = "container" align ="center">
            <h1><center>หน้าจอค้นหาข้อมูลบุคลากร</center></h1>  
        <div class ="row">
        <div class ="col-sm-2" align ="right">
            <label>รหัส</label>
         </div>

        <div class = "col-sm-4">
            <input type="text" name ="personId" id ="personId" class="form-control" >
        </div>
    </div> 
    
        <div class = "row">
             <div class ="col-sm-2" align ="right"><label>ชื่อ</label></div>
            <div class ="col-sm-4"><input type="text" name ="fname" id ="fname" class="form-control"> </div> 
        </div>


        <div class = "row">
            <div class ="col-sm-2" align ="right"><label>นามสกุล</label></div>
            <div class ="col-sm-4"><input type="text" name ="lname" id ="lname" class="form-control"></div>
        </div>

      
        <div class="row">
            <div class="col-sm-2"  align ="right"><label>ระดับการศึกษา :</label></div>
            <div class="col-sm-4"><select name="edu_level_id" id="edu_level_id" class ="form-control"> </select></div>

        </div>
           
        <div class="row" align ="center">
            <div class="col-sm" ><button type="button" name="search"  id="search"  class="btn btn-success">ค้นหา</button></div>
        </div>
    

    <table name ="tbsearch" id ="tbsearch" class="table table-bordered" width ="100%">
            <thead>
                  <tr>
                    <th scope="col" >รหัส</th>
                    <th scope="col" >ชื่อ</th>
                    <th scope="col" >นามสกุล</th>
                    <th scope="col" >รายละเอียด</th>
                
                  </tr>
                </thead>
     </table>
      

     <div class="modal fade" id="myModal">
         <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                 <h2 class="modal-title">รายละเอียดการศึกษา</h2>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                 <table class="table table-striped" id="tblGrid_EduView">
                     <thead>
                        <tr>
                            <th scope="col" >ลำดับ</th>
                            <th scope="col" >ระดับการศึกษา</th> 
                            <th scope="col" >สถานศึกษา</th>
                        </tr>
                    </thead>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default " data-dismiss="modal">Close</button>
            </div>
                                                
        </div>
    </div>
    </div>
 </div>
</form>

@endsection
@section('script')

<script>

     //$(document).ready(function () { 
        var table = $('#tbsearch').DataTable({
                ajax: {
                    url: "search",
                    type: "POST",
                    datatype: "JSON",
                    data: function (d) {
                        return $.extend({}, d, {
                            "personid": $('#personId').val(),
                            "Fname": $('#fname').val(),                            
                            "Lname": $('#lname').val(),                            
                        });
                    },
                    "dataSrc": function (json) {    
                        if (json.data == null) {
                            alert('ไม่พบข้อมูล');
                            return false;
                        } else {
                        return json.data;
                    }
                    },
                    error: function (xhr, error, thrown) {

                    }
                },
                deferLoading: 0,
                processing: true,
                serverSide: true,
                responsive: true,
                columns: [
                   
                    {
                        data: "person_id"
                    },
                    {
                        data: "fname", render: function (data, type, row) {
                            return row["title_id"] + " " + data;
                        }
                    },
                    {
                        data: "lname"
                    } ,   
                    {
                        data: "eduperson", 
                                render : function(data){
                                    return '<a  class="btn btn-outline-success" id ="personEdu_View"    >รายละเอียดการศึกษา</a>';
                                    }
                    },              
                ],
                order: [0, "asc"],
                columnDefs: [
                    {  width: "10%", className: "text-center", targets: [0] },
                    {  width: "35%", className: "text-center", targets: [1] },
                    {  width: "35%", className: "text-center", targets: [2] },
                    {  width: "20%", className: "text-center", targets: [3] }
                ],
                bFilter: false,
                bLengthChange: false,
                ordering: false,
                pageLength: 10
            });


$('#search').click(function() {

        var in_person_id = $('#personId').val();
        var in_fname = $('#fname').val();
        var in_lname = $('#lname').val();
       var in_level_edu = $('#edu_level_id').val();
        if(in_person_id != "" || in_fname != "" || in_lname !="" || in_level_edu != ""){
            table.ajax.reload();
        }else {
            alert("กรุณาระบุเงื่อนไขอย่างน้อยหนึ่งเงื่อนไข")
            $('#personId').focus();
        } 
    });


var table_modal = $('#tblGrid_EduView').DataTable({
                ajax: {
                    url: "getPersonEdu",
                    type: "POST",
                    datatype: "JSON",
                    data: function (d) {
                        return $.extend({}, d, {
                            "personid": $("#tbGrid_EduView").val()     
                                   
                        });
                    }, 
                    "dataSrc": function (json) {    
                        if (json.data == null) {
                            alert('ไม่พบข้อมูลการศึกษา');
                            return false;
                           
                        } else {
                        return json.data;
                      
                    }
                    },
                    error: function (xhr, error, thrown) {

                    }
                },
                deferLoading: 0,
                processing: true,
                serverSide: true,
                responsive: true,
                columns: [
                   
                    {
                        data: "rsd_personEdu"
                    },
                    {
                        data: "rsd_level"
                    },
                    {
                        data: "rsd_personEduName"
                    } ,               
                ],
                order: [0, "asc"],
                columnDefs: [
                    {  width: "10%", className: "text-center", targets: [0] },
                    {  width: "40%", targets: [1] },
                    {  width: "50%", targets: [2] },
                  
                ] ,
                bFilter: false,
                bLengthChange: false,
                ordering: false,
                pageLength: 10
            });


 $('#myModal').on('show.bs.modal', function (e) {
            var personId = $(e.relatedTarget).attr('data-id');
                $("#tbGrid_EduView").val( personId );
                table_modal.ajax.reload();
            });   








//Select2 in DropwnList

$(document).ready(function() {
 $('#edu_level_id').select2();
 $.ajax({
      url:'eduLevel',
      dataType: 'json',
      success: function( json ) {
          $.each(json, function(i, obj){
               $('#edu_level_id').append($('<option>').text(obj.text).attr('value', obj.id));
        });
}
    });

});






</script>
@endsection
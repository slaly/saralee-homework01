<?php
require '../vendor/autoload.php';
$router = new AltoRouter();
$router -> setBasePath('/HOMEWORK01');
$router -> map('GET','/person','','person');
$router -> map('POST','/getdata','','getdata');
$router -> map('POST','/search','','search');
$router -> map('GET','/eduLevel','','eduLevel');
$router -> map('POST','/getPersonEdu','','getPersonEdu');


$match = $router->match();
//var_dump($match);
//exit();
if($match){
    if($match['name']=="person"){
        require_once('../Controllers/PersonController.php');
        $person = new PersonController();
        $person->index();
    }else if($match['name']=="getdata"){
        require_once('../Controllers/PersonController.php'); 
        $person = new PersonController();
        $person->getData();
    }else if($match['name']=="search"){
        require_once('../Controllers/PersonController.php'); 
        $person = new PersonController();
        $person->search();
    }

    else if($match['name']=="eduLevel"){
        require_once('../Controllers/PersonController.php'); 
        $person = new PersonController();
        $person->eduLevel();

    }else if($match['name']=="getPersonEdu"){
        require_once('../Controllers/PersonController.php'); 
        $person = new PersonController();
        $person->getPersonEdu();
    }
   
}else {
    header($_SERVER['SERVER_PROTOCOL'] . '404 Not Found');
    echo "ไม่พบ Page";
    
}

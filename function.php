<?php
require 'vendor/autoload.php';

use Philo\Blade\Blade;
function View ($path,array $data = []) {
	$views = '../Views';
	$cache = '../Cache';
	$blade = new Blade($views, $cache);
	echo $blade->view()->make($path,$data)->render();
} 

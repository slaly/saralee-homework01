var gulp = require('gulp');

var minifyCSS = require('gulp-minify-css');
var renameFile= require('gulp-rename');
var minifyJs = require('gulp-uglify');
var sass = require('gulp-ruby-sass');

var Resasslibjs = [
    './Resources/assets/lib/bootstrap/dist/js/bootstrap.min.js',
    './Resources/assets/lib/datatables/media/js/dataTables.bootstrap4.min.js',
    './Resources/assets/lib/datatables/media/js/jquery.dataTables.min.js',
    './Resources/assets/lib/jquery/dist/jquery.min.js',
    './Resources/assets/lib/jquery-validation/dist/jquery.validate.min.js',
    './Resources/assets/lib/jquery-validation/dist/additional-methods.min.js',
    './Resources/assets/lib/select2/dist/js/select2.min.js',
    './Resources/assets/lib/select2/dist/js/select2.full.min.js'
   
    


];

var Resasslibcss =[
    './Resources/assets/lib/bootstrap/dist/css/bootstrap.min.css',
    './Resources/assets/lib/datatables/media/css/dataTables.bootstrap4.min.css',
    './Resources/assets/lib/datatables/media/css/jquery.dataTables.min.css',
    './Resources/assets/lib/select2/dist/css/select2.min.css'

];

gulp.task('copyResasslibjs', function() {
    return gulp
    .src(Resasslibjs)
    .pipe(gulp.dest('Public/js'));
});

gulp.task('copyResasslibcss', function() {
    return gulp
   .src(Resasslibcss)
   .pipe(gulp.dest('Public/css'));
});
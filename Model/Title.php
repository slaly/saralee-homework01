<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * Title
 *
 * @ORM\Table(name="title")
 * @ORM\Entity
 */
class Title
{
    /**
     * @var int
     *
     * @ORM\Column(name="Title_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $titleId;

    /**
     * @var string
     *
     * @ORM\Column(name="Title_short_name", type="string", length=30, nullable=false)
     */
    private $titleShortName;

    /**
     * @var string
     *
     * @ORM\Column(name="Title_long_name", type="string", length=50, nullable=false)
     */
    private $titleLongName;



    /**
     * Get titleId.
     *
     * @return int
     */
    public function getTitleId()
    {
        return $this->titleId;
    }

    /**
     * Set titleShortName.
     *
     * @param string $titleShortName
     *
     * @return Title
     */
    public function setTitleShortName($titleShortName)
    {
        $this->titleShortName = $titleShortName;

        return $this;
    }

    /**
     * Get titleShortName.
     *
     * @return string
     */
    public function getTitleShortName()
    {
        return $this->titleShortName;
    }

    /**
     * Set titleLongName.
     *
     * @param string $titleLongName
     *
     * @return Title
     */
    public function setTitleLongName($titleLongName)
    {
        $this->titleLongName = $titleLongName;

        return $this;
    }

    /**
     * Get titleLongName.
     *
     * @return string
     */
    public function getTitleLongName()
    {
        return $this->titleLongName;
    }
}
